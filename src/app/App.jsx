import React,{Component} from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';


import Login from '../login/Login.jsx';
import Dashboard from '../dashboard/Dashboard.jsx';
import axios from 'axios';

class App extends Component{

    constructor(props){
        super(props);
        this.state = {
            usuario: null
        };
    }

    fazerLogin = (dados) =>{
        axios.post('http://localhost:10222/', {params:{login: 123456}}).then((response)=>{
            this.setState({usuario: response.data});
        });
        //return this.state.usuairo == null ? <Redirect to="/login"/> : <Redirect to="/dashboard"/>;
    }

    DashboardCall = () =>{
        return this.state.usuario != null ? <Dashboard usuario={this.state.usuario} /> : <Redirect to="/login"/>
    }

    LoginCall = () =>{
        return this.state.usuario == null ? <Login fazerLogin={this.fazerLogin} /> : <Redirect to="/dashboard"/>
    }


    render(){
        return(
            <Router>
                    <Route exact path="/" render={()=>(
                        <Redirect to="/login"/>
                    )}/>
                    <Route exact path="/dashboard" component={this.DashboardCall}/>
                    <Route exact path="/login" component={this.LoginCall}/>
            </Router>
        );
    }
}

export default App;