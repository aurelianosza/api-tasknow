import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {Doughnut} from 'react-chartjs';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import Progress from 'react-progressbar';

import '../libs/bootstrap/dist/css/bootstrap.min.css';
import './Dashboard.css'


class Dashboard extends Component{

    constructor(props){
        super(props);
        var data = [
            {
                value: this.props.usuario.horas_aproveitadas_totais,
                color:"#6b00b3",
                highlight: "#6d02b5",
                label: "Horas aproveitadas"
            },
            {
                value: this.props.usuario.grade.limite - this.props.usuario.horas_aproveitadas_totais,
                color: "#cccccc",
                highlight: "#dddddd",
                label: "Horas restantes"
            }
        ];
        this.state={data:data}

        library.add([faBars,faTimes ]);


    }

    abrirSidebar = ()=>{
        document.getElementById("my-sidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
    }

    fecharSidebar = ()=>{
        document.getElementById("my-sidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
    }

    mostrarCategorias = ()=>{
        return this.props.usuario.grade.categorias.map((categoria)=>{
            var totalCategoria = 0;
            this.props.usuario.atividades.map((atividade)=>{
                if(atividade.id_categoria == categoria.id){
                    totalCategoria = totalCategoria + atividade.quantidade_aproveitada;
                }
            });
            return (<div>
                {categoria.nome}
                <Progress completed={ 100*totalCategoria/categoria.limite} /><span>{totalCategoria}/{categoria.limite}</span>
            </div>);
        });
    }

    render(){
        if(this.props.usuario == null)
            return <Redirect to="/login "/>

        return(
            <div>
                <div id="my-sidenav" className="sidenav">
                    <a href="javascript:void(0)" className="closebtn" onClick={this.fecharSidebar}>
                        <FontAwesomeIcon icon="times"/>
                    </a>
                    <a href="#">Minhas horas</a>
                    <a href="#">Informações</a>
                </div>
                <nav class="navbar navbar-light bg-primary">
                    <a href="#" onClick={this.abrirSidebar} className="icone-side-bar">
                        <FontAwesomeIcon icon="bars" />
                    </a>
                </nav>
                <div id="main">
                    <div className="container">
                        <div className="row">
                            <div className="col col-12 col-md-8 offset-md-2">
                                <div className="card">
                                    <div className="card-header bg-primary text-white">
                                    <h5>
                                        Veja aqui suas horas aproveitadas.
                                    </h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="text-center">
                                            <div>
                                                <Doughnut data={this.state.data}  width="400" height="250"/>
                                            </div>
                                            <div>
                                                <h2>
                                                    { 100*(this.props.usuario.horas_aproveitadas_totais /this.props.usuario.grade.limite ).toFixed(2)}%
                                                </h2>
                                            </div>
                                        </div>
                                        <hr/>
                                            Ver horas por categorias
                                        <hr/>
                                        <div>
                                            {this.mostrarCategorias()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;