import React, {Component} from 'react';

import '../libs/bootstrap/dist/css/bootstrap.min.css'; 
import './Login.css';

class Login extends Component{

    constructor(props){
        super(props);

        this.state = {
            matricula : '',
            senha : ''
        }

    }

    fazerLogin = (event) =>{
        event.preventDefault();
        this.props.fazerLogin({matricula: this.state.matricula, senha: this.state.senha});
    }


    render(){
        return(
            <div>
                <section id="login" className="">
                    <div className="container">
                        <div className="row">
                            <div className="col col-12 col-md-8 col-lg-4 offset-md-2 offset-lg-4">
                            <div className="card">
                                <div className="card-body">
                                    <form onSubmit={this.fazerLogin}>
                                        <div className="form-group">
                                            <label htmlFor="login">Matricula </label>
                                            <input type="text" className="form-control" onChange={(event) => {this.setState({matricula:event.target.value})}} name="matricula" value={this.state.matricula}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="senha">Senha</label>
                                            <input type="password" className="form-control" onChange={(event)=>{this.setState({senha:event.target.value})}} name="senha" value={this.state.senha}/>
                                        </div>
                                        <button className="btn btn-primary btn-block btn-lg">Entrar no sistema</button>
                                    </form>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Login;